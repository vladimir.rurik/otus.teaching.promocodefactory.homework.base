﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Employees
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Get data of all employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Get employee data by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Create a new employee
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(EmployeeRequest request)
        {
            var newEmployee = await MapEmployeeRequestToEmployeeAsync(request);
            var created = await _employeeRepository.CreateAsync(newEmployee);

            var result = MapEmployeeToEmployeeResponse(created);
            return Ok(result);
        }

        /// <summary>
        /// Update employee data by his Id
        /// </summary>
        /// <param name="request">Employee data</param>
        /// <returns>Updated employee data</returns>
        [HttpPut("")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(EmployeeRequest request)
        {
            var employee = await _employeeRepository.GetByIdAsync(request.Id);

            if (employee == null)
            {
                return NotFound();
            }

            var newEmployee = await MapEmployeeRequestToEmployeeAsync(request);
            var updated = await _employeeRepository.UpdateAsync(newEmployee);

            var result = MapEmployeeToEmployeeResponse(updated);
            return Ok(result);
        }

        /// <summary>
        /// Delete employee from the database by Id
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            try
            {
                await _employeeRepository.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok("Employee have been deleted.");
        }

        private async Task<Employee> MapEmployeeRequestToEmployeeAsync(EmployeeRequest request)
        {
            List<Role> roles = new();
            foreach (var roleId in request.RoleIds.Distinct())
            {
                var role = await _roleRepository.GetByIdAsync(Guid.Parse(roleId));
                roles.Add(role);
            }

            return new Employee
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                AppliedPromocodesCount = request.AppliedPromocodesCount,
                Roles = roles
            };
        }

        private EmployeeResponse MapEmployeeToEmployeeResponse(Employee model)
        {
            return new EmployeeResponse()
            {
                Id = model.Id,
                Email = model.Email,
                Roles = model.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = model.FullName,
                AppliedPromocodesCount = model.AppliedPromocodesCount
            };
        }
    }
}
