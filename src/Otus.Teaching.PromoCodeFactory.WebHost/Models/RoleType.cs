﻿using System.ComponentModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public enum RoleType
    {
        Admin = 1,

        PartnerManager = 2
    }
}