﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data;
        }

        public Task<ICollection<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        public Task<T> CreateAsync(T item)
        {
            Data.Add(item);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task<T> UpdateAsync(T item)
        {
            var entity = Data.FirstOrDefault(x => x.Id == item.Id);
            Data.Remove(entity);
            Data.Add(item);

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task DeleteAsync(Guid id)
        {
            var entity = Data.FirstOrDefault(x => x.Id == id);
            Data.Remove(entity);

            return Task.FromResult<object>(null);
        }
    }
}